#include "AstroSignRequest.h"

AstroSignRequest::AstroSignRequest(const int luna, const int zi, const int an) : Request("AstroSign")
{
	this->content.push_back(boost::property_tree::ptree::value_type("Luna", std::to_string(luna)));
	this->content.push_back(boost::property_tree::ptree::value_type("Zi", std::to_string(zi)));
	this->content.push_back(boost::property_tree::ptree::value_type("An", std::to_string(an)));
}

