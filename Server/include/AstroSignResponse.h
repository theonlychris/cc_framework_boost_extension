#pragma once
#include "Response.h"

class AstroSignResponse : public Framework::Response
{
public:
	AstroSignResponse();
	std::string getSign(const int luna, const int zi);
	std::string interpretPacket(const boost::property_tree::ptree& packet);
};

