#include "AstroSignResponse.h"



AstroSignResponse::AstroSignResponse() : Response("AstroSign")
{}

std::string AstroSignResponse::getSign(const int luna, const int zi)
{
	std::string sign;

	if (luna == 12)
	{
		if (zi < 22)
			sign = "Sagetator (Nov 22-Dec 21)";
		else
			sign = "Capricorn (Dec 22-Jan 19)";
	}
	else if (luna == 1)
	{
		if (zi < 20)
			sign = "Capricorn (Dec 22-Jan 19)";
		else
			sign = "Varasator (Jan 20-Feb 18)";
	}
	else if (luna == 2)
	{
		if (zi < 19)
			sign = "Varasator (Jan 20-Feb 18)";
		else
			sign = "Pesti (Feb 19-Mar 20)";
	}
	else if (luna == 3)
	{
		if (zi < 21)
			sign = "Pesti (Feb 19-Mar 20)";
		else
			sign = "Berbec (Mar 21-Apr 20)";
	}
	else if (luna == 4)
	{
		if (zi < 20)
			sign = "Berbec (Mar 21-Apr 20)";
		else
			sign = "Taur (Apr 21-May 20)";
	}
	else if (luna == 5)
	{
		if (zi < 21)
			sign = "Taur (Apr 21-May 20)";
		else
			sign = "Gemeni (May 21-Jun 21)";
	}
	else if (luna == 6)
	{
		if (zi < 21)
			sign = "Gemeni (May 21-Jun 21)";
		else
			sign = "Rac (Jun 22 - Jul 22)";
	}
	else if (luna == 7)
	{
		if (zi < 23)
			sign = "Rac (Jun 22-Jul 22)";
		else
			sign = "Leu (Jul 23-22 Aug)";
	}
	else if (luna == 8)
	{
		if (zi < 23)
			sign = "Leu (Jul 23-22 Aug)";
		else
			sign = "Fecioara (Aug 23-Sep 22)";
	}
	else if (luna == 9)
	{
		if (zi < 23)
			sign = "Fecioara (Aug 23-Sep 22)";
		else
			sign = "Balanta (Sep 23-Oct 22)";
	}
	else if (luna == 10)
	{
		if (zi < 23)
			sign = "Balanta (Sep 23-Oct 22)";
		else
			sign = "Scorpion (Oct 23-Nov 21)";
	}
	else if (luna == 11)
	{
		if (zi < 22)
			sign = "Scorpion (Oct 23-Nov 21)";
		else
			sign = "Sagetator (Nov 22-Dec 21)";
	}

	return sign;
}

std::string AstroSignResponse::interpretPacket(const boost::property_tree::ptree& packet)
{
	auto zi = atoi(packet.get<std::string>("Zi").c_str());
	auto luna = atoi(packet.get<std::string>("Luna").c_str());
	auto an = atoi(packet.get<std::string>("An").c_str());

	this->content.push_back(boost::property_tree::ptree::value_type("File", "AstroSign.txt"));

	std::string sign = getSign(luna, zi);
	this->content.push_back(boost::property_tree::ptree::value_type("Result", sign));

	// if (luna == 3 || luna == 4)
	//	if ((luna == 3 && zi >= 21) || (luna == 4 && zi <= 19))

	return this->getContentAsString();
}
